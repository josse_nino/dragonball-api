package com.desin.dragonball.repository;

import com.desin.dragonball.model.Planet;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface PlanetRepository extends MongoRepository<Planet, String> {

    /**
     * find planet by name
     * @param name
     * @return
     */
    Optional<Planet> findByName(String name);
}
