package com.desin.dragonball.repository;

import com.desin.dragonball.model.Character;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CharacterRepository extends MongoRepository<Character, String> {

    /**
     * find character by name
     * @param name
     * @return
     */
    Optional<Character> findByName(String name);
}
