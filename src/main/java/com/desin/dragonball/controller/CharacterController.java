package com.desin.dragonball.controller;


import com.desin.dragonball.model.Character;
import com.desin.dragonball.service.CharacterService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CharacterController {

    @Autowired
    private CharacterService characterService;

    @Operation(summary = "Get all Dragon Ball characters ", description = "Get all Dragon ball characters registered")
    @ApiResponse(responseCode = "200", description = "Found the characters", content = @Content(
            mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Character.class))
    ))
    @ApiResponse(responseCode = "404", description = "Not found characters")
    @GetMapping("/character/all")
    public ResponseEntity<List<Character>> getAll(){
        return characterService.getAll();
    }

    @Operation(summary = "Get one character", description = "Find character by id")
    @Parameter(in = ParameterIn.PATH , name = "id", required = true, description = "Character id",
                schema = @Schema(implementation = String.class))
    @ApiResponse(responseCode = "200", description = "Found character", content = @Content(
            mediaType = "application/json", schema = @Schema(implementation = Character.class)
    ))
    @ApiResponse(responseCode = "404", description = "Not found characters")
    @GetMapping("/character/{id}")
    public ResponseEntity<Character> getById(@PathVariable("id") String id){
        return characterService.getById(id);
    }

    @Operation(summary = "Get one character", description = "Find character by name")
    @Parameter(in = ParameterIn.PATH , name = "name", required = true, description = "Character's name, Example: Goku",
            schema = @Schema(implementation = String.class))
    @ApiResponse(responseCode = "200", description = "Found character", content = @Content(
            mediaType = "application/json", schema = @Schema(implementation = Character.class)
    ))
    @ApiResponse(responseCode = "404", description = "Not found characters")
    @GetMapping("/character/name/{name}")
    public ResponseEntity<Character> getByName(@PathVariable("name") String name){
        return characterService.getByName(name);
    }

    @Operation(summary = "Save character", description = "Save characters in the system, only registered users can save")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Character object", required = true,
        content = @Content(mediaType = "application/json", schema = @Schema(implementation = Character.class))
    )
    @Parameter(in = ParameterIn.PATH , name = "id", required = true, description = "User id",
            schema = @Schema(implementation = String.class))
    @ApiResponse(responseCode = "404", description = "Not found characters")
    @ApiResponse(responseCode = "200", description = "Saved character", content = @Content(
            mediaType = "application/json", schema = @Schema(implementation = Character.class)
    ))
    @PostMapping("/character/{id}")
    public ResponseEntity<Character> save(@RequestBody Character character, @PathVariable("id") String id){
        return characterService.save(character, id);
    }

    @Operation(summary = "Save many characters", description = "Save characters in the system, only registered users can save")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Character object", required = true,
            content = @Content(mediaType = "application/json", array = @ArraySchema( schema = @Schema(implementation = Character.class)))
    )
    @Parameter(in = ParameterIn.PATH , name = "id", required = true, description = "User id",
            schema = @Schema(implementation = Integer.class))
    @ApiResponse(responseCode = "200", description = "Saved character", content = @Content(
            mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Character.class))
    ))
    @ApiResponse(responseCode = "404", description = "Not found characters")
    @PostMapping("/characters/{id}")
    public ResponseEntity<List<Character>> saveAll(@RequestBody List<Character> characters, @PathVariable("id") String id){
        return characterService.saveAll(characters, id);
    }
}
