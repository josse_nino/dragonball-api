package com.desin.dragonball.controller;

import com.desin.dragonball.model.Planet;
import com.desin.dragonball.service.PlanetService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PlanetController {

    @Autowired
    private PlanetService planetService;

    @Operation(summary = "Get all Dragon Ball planets ", description = "Get all Dragon ball planets registered")
    @ApiResponse(responseCode = "200", description = "Found the planets", content = @Content(
            mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Planet.class))
    ))
    @ApiResponse(responseCode = "404", description = "Not found planets")
    @GetMapping("/planet/all")
    public ResponseEntity<List<Planet>> getAll(){
        return planetService.getAll();
    }

    @Operation(summary = "Get one planet", description = "Find planet by id")
    @Parameter(in = ParameterIn.PATH , name = "id", required = true, description = "Planet id",
            schema = @Schema(implementation = String.class))
    @ApiResponse(responseCode = "200", description = "Found planet", content = @Content(
            mediaType = "application/json", schema = @Schema(implementation = Planet.class)
    ))
    @ApiResponse(responseCode = "404", description = "Not found planet")
    @GetMapping("/planet/{id}")
    public ResponseEntity<Planet> getById(@PathVariable("id") String id){
        return planetService.getById(id);
    }

    @Operation(summary = "Get one planet", description = "Find planet by name")
    @Parameter(in = ParameterIn.PATH , name = "name", required = true, description = "Planet name, Example: Goku",
            schema = @Schema(implementation = String.class))
    @ApiResponse(responseCode = "200", description = "Found planet", content = @Content(
            mediaType = "application/json", schema = @Schema(implementation = Planet.class)
    ))
    @ApiResponse(responseCode = "404", description = "Not found planet")
    @GetMapping("/planet/name/{name}")
    public ResponseEntity<Planet> getByName(@PathVariable("name") String name){
        return planetService.getByName(name);
    }

    @Operation(summary = "Save planet", description = "Save planets in the system, only registered users can save")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Planet object", required = true,
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Planet.class))
    )
    @Parameter(in = ParameterIn.PATH , name = "id", required = true, description = "User id",
            schema = @Schema(implementation = String.class))
    @ApiResponse(responseCode = "404", description = "Not found planet")
    @ApiResponse(responseCode = "200", description = "Saved planet", content = @Content(
            mediaType = "application/json", schema = @Schema(implementation = Character.class)
    ))
    @PostMapping("/planet/{id}")
    public ResponseEntity<Planet> save(@RequestBody Planet planet, @PathVariable("id") String id){
        return planetService.save(planet, id);
    }

    @Operation(summary = "Save many planets", description = "Save planets in the system, only registered users can save")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Character object", required = true,
            content = @Content(mediaType = "application/json", array = @ArraySchema( schema = @Schema(implementation = Character.class)))
    )
    @Parameter(in = ParameterIn.PATH , name = "id", required = true, description = "User id",
            schema = @Schema(implementation = Integer.class))
    @ApiResponse(responseCode = "200", description = "Saved planet", content = @Content(
            mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Planet.class))
    ))
    @ApiResponse(responseCode = "404", description = "Not found planets")
    @PostMapping("/planets/{id}")
    public ResponseEntity<List<Planet>> save(@RequestBody List<Planet> planets, @PathVariable("id") String id){
        return planetService.saveAll(planets, id);
    }
}
