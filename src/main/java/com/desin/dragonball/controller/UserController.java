package com.desin.dragonball.controller;

import com.desin.dragonball.model.User;
import com.desin.dragonball.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Operation(summary = "Get user registered", description = "Find user by email ")
    @ApiResponse(responseCode = "200", description = "Found user", content = @Content(
            mediaType = "application/json", schema = @Schema(implementation = User.class)
    ))
    @ApiResponse(responseCode = "404", description = "Not found user")
    @GetMapping("/user/{email}")
    public ResponseEntity<User> getUserByEmail(@PathVariable("email") String email){
        return userService.getUserByEmail(email);
    }

    @Operation(summary = "Get user registered", description = "Find user by id")
    @Parameter(in = ParameterIn.PATH , name = "id", required = true, description = "Planet id",
            schema = @Schema(implementation = String.class))
    @ApiResponse(responseCode = "200", description = "Found user", content = @Content(
            mediaType = "application/json", schema = @Schema(implementation = User.class)
    ))
    @ApiResponse(responseCode = "404", description = "Not found user")
    @GetMapping("/user/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") String id){
        return userService.getUserById(id);
    }

    @Operation(summary = "Save user", description = "Save user in the system, only registered users can save")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "User object", required = true,
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = User.class))
    )
    @ApiResponse(responseCode = "404", description = "Not found user")
    @ApiResponse(responseCode = "200", description = "Saved user", content = @Content(
            mediaType = "application/json", schema = @Schema(implementation = User.class)
    ))
    @PostMapping("/user/create")
    public ResponseEntity<User> saveUser(@RequestBody User user){
        return userService.saveUser(user);
    }
}
