package com.desin.dragonball.exceptions;

public class PlanetAlreadyCreatedException extends RuntimeException {
    public PlanetAlreadyCreatedException(String name){
        super(String.format("Planet with name %s already exits", name));
    }
}
