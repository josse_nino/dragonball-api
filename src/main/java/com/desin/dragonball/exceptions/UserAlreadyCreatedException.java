package com.desin.dragonball.exceptions;

public class UserAlreadyCreatedException extends RuntimeException {
    public UserAlreadyCreatedException(String name){
        super(String.format("User with email %s already exits", name));
    }
}
