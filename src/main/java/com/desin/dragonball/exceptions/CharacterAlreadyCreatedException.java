package com.desin.dragonball.exceptions;

public class CharacterAlreadyCreatedException  extends RuntimeException {
    public CharacterAlreadyCreatedException(String name){
        super(String.format("Character with name %s already exits", name));
    }
}
