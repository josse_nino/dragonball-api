package com.desin.dragonball.service;

import com.desin.dragonball.model.Planet;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface PlanetService {

    ResponseEntity<List<Planet>> getAll();

    ResponseEntity<Planet> getById(String id);

    ResponseEntity<Planet> getByName(String name);

    ResponseEntity<Planet> save(Planet planet, String id);

    ResponseEntity<List<Planet>> saveAll(List<Planet> planets, String id);
}
