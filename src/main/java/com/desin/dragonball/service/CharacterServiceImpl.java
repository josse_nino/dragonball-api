package com.desin.dragonball.service;

import com.desin.dragonball.exceptions.CharacterAlreadyCreatedException;
import com.desin.dragonball.exceptions.CharacterNotFoundException;
import com.desin.dragonball.exceptions.UserNotFoundException;
import com.desin.dragonball.model.Character;
import com.desin.dragonball.model.User;
import com.desin.dragonball.repository.CharacterRepository;
import com.desin.dragonball.repository.UserRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharacterServiceImpl implements CharacterService {

    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public ResponseEntity<List<Character>> getAll() {
        List<Character> characters = null;
        try {
            characters = characterRepository.findAll();
            return new ResponseEntity<>(characters, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(characters, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Character> getById(String id) {
        Character character = null;
        try {
            character = characterRepository.findById(String.valueOf(new ObjectId(id))).orElseThrow(CharacterNotFoundException::new);
            return new ResponseEntity<>(character, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(character, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Character> getByName(String name) {
        Character character = null;
        try {
            character = characterRepository.findByName(name).orElseThrow(CharacterNotFoundException::new);
            return new ResponseEntity<>(character, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(character, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Character> save(Character character, String id) {
        try {
            User user = userRepository.findById(String.valueOf(new ObjectId(id))).orElseThrow(UserNotFoundException::new);
            if(user != null){
                if(character != null && character.getName() != null && !"".equalsIgnoreCase(character.getName())){
                    characterRepository.findByName(character.getName()).ifPresentOrElse(
                            existingCharacter -> {
                                throw new CharacterAlreadyCreatedException(character.getName());
                            },
                            () -> characterRepository.save(character)
                    );
                    return new ResponseEntity<>(character, HttpStatus.OK);
                }else{
                    return new ResponseEntity<>(character, HttpStatus.NOT_FOUND);
                }
            }else{
                return new ResponseEntity<>(character, HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(character, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<Character>> saveAll(List<Character> characters, String id) {
        try {
            User user = userRepository.findById(String.valueOf(new ObjectId(id))).orElseThrow(UserNotFoundException::new);
            if(user != null){
                characters.forEach(character -> {
                    if(character != null && character.getName() != null && !"".equalsIgnoreCase(character.getName())){
                        characterRepository.findByName(character.getName()).ifPresentOrElse(
                                existingCharacter -> {
                                    throw new CharacterAlreadyCreatedException(character.getName());
                                },
                                () -> characterRepository.save(character)
                        );
                    }
                });
                return new ResponseEntity<>(characters, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(characters, HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(characters, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
