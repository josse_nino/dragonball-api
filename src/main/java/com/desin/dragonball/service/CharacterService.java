package com.desin.dragonball.service;

import com.desin.dragonball.model.Character;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CharacterService {

    ResponseEntity<List<Character>> getAll();

    ResponseEntity<Character> getById(String id);

    ResponseEntity<Character> getByName(String name);

    ResponseEntity<Character> save(Character character, String id);

    ResponseEntity<List<Character>> saveAll(List<Character> characters, String id);
}
