package com.desin.dragonball.service;

import com.desin.dragonball.exceptions.UserAlreadyCreatedException;
import com.desin.dragonball.exceptions.UserNotFoundException;
import com.desin.dragonball.model.User;
import com.desin.dragonball.repository.UserRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public ResponseEntity<User> getUserById(String id) {
        User user = null;
        try {
            user = userRepository.findById(String.valueOf(new ObjectId(id))).orElseThrow(UserNotFoundException::new);;
            return new ResponseEntity<>(user, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(user, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<User> getUserByEmail(String email) {
        User user = null;
        try {
            user = userRepository.findByEmail(email).orElseThrow(UserNotFoundException::new);
            return new ResponseEntity<>(user, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(user, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<User> saveUser(User user) {
        try {
            if(user != null && validateRequestData(user)){
                userRepository.findByEmail(user.getEmail()).ifPresentOrElse(
                        existingCharacter -> {
                            throw new UserAlreadyCreatedException(user.getEmail());
                        },
                        () -> userRepository.save(user)
                );
                return new ResponseEntity<>(user, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(user, HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(user, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private boolean validateRequestData(User user){
        boolean validation = true;
        if(user.getEmail() == null || "".equalsIgnoreCase(user.getEmail())){
            validation = false;
        }else if(user.getName() == null || "".equalsIgnoreCase(user.getName())){
            validation = false;
        }else if(user.getLastName() == null || "".equalsIgnoreCase(user.getLastName())){
            validation = false;
        }else if(user.getPassword() == null || "".equalsIgnoreCase(user.getPassword()) || user.getPassword().length() < 5){
            validation = false;
        }
        return validation;
    }
}
