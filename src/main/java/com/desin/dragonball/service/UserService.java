package com.desin.dragonball.service;

import com.desin.dragonball.model.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

    ResponseEntity<User> getUserById(String id);

    ResponseEntity<User> getUserByEmail(String email);

    ResponseEntity<User> saveUser(User user);
}
