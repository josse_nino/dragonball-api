package com.desin.dragonball.service;

import com.desin.dragonball.exceptions.CharacterNotFoundException;
import com.desin.dragonball.exceptions.PlanetAlreadyCreatedException;
import com.desin.dragonball.exceptions.PlanetNotFoundException;
import com.desin.dragonball.exceptions.UserNotFoundException;
import com.desin.dragonball.model.Planet;
import com.desin.dragonball.model.User;
import com.desin.dragonball.repository.PlanetRepository;
import com.desin.dragonball.repository.UserRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlanetServiceImpl implements PlanetService {

    @Autowired
    PlanetRepository planetRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public ResponseEntity<List<Planet>> getAll() {
        List<Planet> planets = null;
        try {
            planets = planetRepository.findAll();
            return new ResponseEntity<>(planets, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(planets, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Planet> getById(String id) {
        Planet planet = null;
        try {
            planet = planetRepository.findById(String.valueOf(new ObjectId(id))).orElseThrow(PlanetNotFoundException::new);
            return new ResponseEntity<>(planet, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(planet, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Planet> getByName(String name) {
        Planet planet = null;
        try {
            planet = planetRepository.findByName(name).orElseThrow(PlanetNotFoundException::new);
            return new ResponseEntity<>(planet, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(planet, HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<Planet> save(Planet planet, String id) {
        try {
            User user = userRepository.findById(String.valueOf(new ObjectId(id))).orElseThrow(UserNotFoundException::new);
            if(user != null){
                if(planet != null && planet.getName() != null && !"".equalsIgnoreCase(planet.getName())){
                    planetRepository.findByName(planet.getName()).ifPresentOrElse(
                            existingCharacter -> {
                                throw new PlanetAlreadyCreatedException(planet.getName());
                            },
                            () -> planetRepository.save(planet)
                    );
                    return new ResponseEntity<>(planet, HttpStatus.OK);
                }else{
                    return new ResponseEntity<>(planet, HttpStatus.NOT_FOUND);
                }
            }else{
                return new ResponseEntity<>(planet, HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(planet, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<List<Planet>> saveAll(List<Planet> planets, String id) {
        try {
            User user = userRepository.findById(String.valueOf(new ObjectId(id))).orElseThrow(UserNotFoundException::new);
            if(user != null){
                planets.forEach(planet -> {
                    if(planet != null && planet.getName() != null && !"".equalsIgnoreCase(planet.getName())){
                        planetRepository.findByName(planet.getName()).ifPresentOrElse(
                                existingCharacter -> {
                                    throw new PlanetAlreadyCreatedException(planet.getName());
                                },
                                () -> planetRepository.save(planet)
                        );
                    }
                });
                return new ResponseEntity<>(planets, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(planets, HttpStatus.NOT_FOUND);
            }
        }catch (Exception e){
            return new ResponseEntity<>(planets, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
