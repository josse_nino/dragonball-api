# Dragon Ball API

The dragon ball api is an application with which you can obtain information about this great anime.

The information provided by the application are

- Characters with their transformations
- Planets with their inhabitants


## Installation

Use java 11 and maven.

```bash
./mvnw package spring-boot:run
```

## Usage

```bash
To consume this api you can do it 
from the following url or by downloading the project
```
[heroku-api]

## Documentation
The documentation is created by swagger and can be accessed through

[Swagger]


Please use the documentation correctly.

## Creator
Josse Niño CEO [DesIn]


## License
[MIT](https://choosealicense.com/licenses/mit/)

[heroku-api]: <https://dragonball-api.herokuapp.com/dragonball/api>
[Swagger]: <https://dragonball-api.herokuapp.com/dragonball/api/swagger-ui.html>
[DesIn]: <http://desinsoft.com/>